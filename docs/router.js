import fs from 'fs'
import { Router } from 'director'

const loadPage = (html) => {
    return (a) => { document.getElementById('page').innerHTML = html }
}

const routes = {
    '/': loadPage(fs.readFileSync(__dirname + '/home.html', 'utf8')),

    '/components/nav': loadPage(fs.readFileSync(__dirname + '/components/nav.html', 'utf8')),

    '/elements/title': loadPage(fs.readFileSync(__dirname + '/elements/title.html', 'utf8')),
};

let active = [];

const options = {
    notfound: loadPage(fs.readFileSync(__dirname + '/404.html', 'utf8')),
    before() {
        active.forEach(el => {
            el.classList.remove('active');
        });
        active = [];
    },
    on() {
        let path = window.location.hash.split('/');
        while (path.length > 1) {
            let activate = document.querySelectorAll(`[href="${path.join('/')}"]`);
            activate.forEach(el => {
                el.classList.add('active');
                active.push(el);
            })
            path.pop();
        }
        
    }
}

const router = Router(routes).configure(options);

router.init(['/']);

if (module.hot) {
    module.hot.dispose(() => {
        router.destroy();
    });
}